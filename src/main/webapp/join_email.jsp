<%-- 
    Document   : join_email.jsp
    Created on : 15-Jan-2014, 9:11:54 PM
    Author     : Ken
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Standard Servlet/JSP Example</title>
        <link rel="stylesheet" href="styles/main.css" type="text/css"/>
    </head>
    <body>
        <h1>Join our email list</h1>
        <p>To join our email list, enter your name and
            email address below.</p>

        <form action="AddToEmailList" method="post">
            <label class="pad_top">Email:</label>
            <input type="email" name="email" value="${user.email}" required><br>
            <label class="pad_top">First Name:</label>
            <input type="text" name="firstName" value="${user.firstName}" required><br>
            <label class="pad_top">Last Name:</label>
            <input type="text" name="lastName" value="${user.lastName}" required><br>
            <label>&nbsp;</label>
            <input type="submit" value="Join Now" class="margin_left">
        </form>
    </body>
</html>