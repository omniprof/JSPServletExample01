<%-- 
    Document   : display_email.jsp
    Created on : 15-Jan-2014, 9:11:54 PM
    Author     : Ken
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Standard Servlet/JSP Example</title>
        <link rel="stylesheet" href="styles/main.css" type="text/css"/>
    </head>

    <body>
        <h1>Thanks for joining our email list</h1>

        <p>Here is the information that you entered:</p>
        <label>Email:</label>
        <span>${user.email}</span><br>
        <label>First Name:</label>
        <span>${user.firstName}</span><br>
        <label>Last Name:</label>
        <span>${user.lastName}</span><br>

        <p>To enter another email address, click on the Return button shown below.</p>

        <form action="join_email.jsp" method="post">
            <input type="submit" value="Return">
        </form>
    </body>




</html>