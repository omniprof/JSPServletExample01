package com.kfwebstandard.jspservletexample01.servlet;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.kfwebstandard.jspservletexample01.model.User;
import com.kfwebstandard.jspservletexample01.persistence.UserIO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Ken
 */
@WebServlet(name = "AddToEmailList", urlPatterns = {"/AddToEmailList"})
public class AddToEmailListServlet extends HttpServlet {

    private final static Logger LOG = LoggerFactory.getLogger(AddToEmailListServlet.class);

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String url = "/display_email.jsp";
        // get parameters from the request
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String email = request.getParameter("email");

        // store data in User object
        User user = new User(firstName, lastName, email);

        // Read the context param from web.xml
        ServletContext context = getServletContext();
        String fileName = context.getInitParameter("EmailFile");

        // write the User object to a file
        UserIO userIO = new UserIO();
        userIO.addRecord(user, fileName);

        request.setAttribute("user", user);
        // forward request and response to JSP page
        RequestDispatcher dispatcher
                = getServletContext().getRequestDispatcher(url);
        dispatcher.forward(request, response);
    }
}
